#include <arpa/inet.h>
#include <ctype.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

static const size_t max_word_len = 255; // should be enough for almost all words

enum request_type {
    SINGLE_LETTER,
    WHOLE_WORD,
    RESTART
};

// argv[1] - IPv4 address, argv[2] - port
int main(int argc, char** argv) {
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    struct sockaddr_in server_addr =
        {AF_INET, htons(strtol(argv[2], NULL, 10)), {inet_addr(argv[1])}};

    if (connect(socket_fd, (void*)&server_addr, sizeof(server_addr)) != 0) {
        close(socket_fd);
        perror("connect");
        exit(EXIT_FAILURE);
    }

    char request[max_word_len + 4]; // interaction protocol + '\0

    request[0] = RESTART;
    write(socket_fd, request, 1);
    
    int game_over = 0;
    
    /* interaction protocol:
    [0] - word_guessed - game_over
    [1] - attempts left
    [2] - word length */
    
    while (!game_over) {
        read(socket_fd, request, sizeof(request));
        write(1, request + 3, request[2]);
        printf("\n%u attempts left\n", request[1]);
    
        if (request[0] == 1 || request[1] == 0) {
            if (request[0] == 1) {
                puts("Congratulations!\nYou guessed the word");
            } else {
                puts("What a pity!\nYou lost");
            }
            char ans = 0;
            while (ans == 0) {
                printf("Want to play again? [y/n]: ");
                fflush(stdout);
                ans = getchar();
                getchar(); // discard '\n'
                if (ans == 'n' || ans == 'N') {
                    game_over = 1;
                } else if (ans == 'y' || ans == 'Y') {
                    request[0] = RESTART;
                    write(socket_fd, request, 1);
                } else {
                    ans = 0;
                }
            }
        } else {
            char ans = 0;
            while (ans == 0) {
                printf("Enter 1 to guess single letter, or 2 to guess the whole word [1/2]: ");
                fflush(stdout);
                ans = getchar();
                getchar(); // discard '\n'
                if (ans == '1') {
                    char letter = 0;
                    while (letter == 0) {
                        printf("Enter letter from 'a'-'z', 'A'-'Z': ");
                        fflush(stdout);
                        letter = getchar();
                        getchar(); // discard '\n'
                        if (!isalpha(letter)) {
                            letter = 0;
                        } else {
                            letter = tolower(letter);
                            request[0] = SINGLE_LETTER;
                            request[1] = letter;
                            write(socket_fd, request, 2);
                        }
                    }
                } else if (ans == '2') {
                    request[0] = WHOLE_WORD;
                    printf("Enter the word: ");
                    fflush(stdout);
                    scanf("%s", request + 1);
                    getchar(); // discard '\n'
                    write(socket_fd, request, 1 + strlen(request + 1));
                } else {
                    ans = 0;
                }
            }
        }
    }

    shutdown(socket_fd, SHUT_RDWR);
    close(socket_fd);
    return 0;
}
